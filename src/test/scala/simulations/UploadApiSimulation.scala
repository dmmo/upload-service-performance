package simulations

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.scenario.Simulation

import scala.io.Source

class UploadApiSimulation extends Simulation with SimulationConfig {

  private val data = Source.fromResource("twitter.json").mkString
  private val sourceId = "5a098fc7e4b043bb5d4c58d8"
  private val uploadUrl = s"/api/upload/$sourceId"

  private val httpConf = http
    .baseURL("http://localhost:8080/zoomdata")
    .basicAuth(username, password)

  private val scn = scenario("Upload API")
      .during(1 minute) {
        exec(
          http("Upload JSON")
            .post(uploadUrl)
            .body(StringBody(data))
            .header("Content-Type", "application/json")
            .check(status is 200)
        ).pause(5 millis)
      }

  setUp(scn.inject(atOnceUsers(10)))
    .protocols(httpConf)
    .assertions(global.successfulRequests.percent.is(100))
}
