package simulations

import com.typesafe.config.ConfigException
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

trait SimulationConfig extends LazyLogging {

  import com.typesafe.config.Config
  import com.typesafe.config.ConfigFactory

  val conf: Config = ConfigFactory.load

  /**
    * Gets the required string from the config file or throws
    * an exception if the string is not found.
    *
    * @param path path to string
    * @return string fetched by path
    */
  def getRequiredString(path: String): String = {
    Try(conf.getString(path)).getOrElse {
      handleError(path)
    }
  }

  /**
    * Gets the required int from the config file or throws
    * an exception if the int is not found.
    *
    * @param path path to int
    * @return int fetched by path
    */
  def getRequiredInt(path: String): Int = {
    Try(conf.getInt(path)).getOrElse {
      handleError(path)
    }
  }

  private[this] def handleError(path: String) = {
    val errMsg = s"Missing required configuration entry: $path"
    logger.error(errMsg)
    throw new ConfigException.Missing(errMsg)
  }

  val username: String = getRequiredString("service.username")

  val password: String = getRequiredString("service.password")



}
