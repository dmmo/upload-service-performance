# Upload API Performance Tests #

This projects contains Gatling simulation tests to exercise performance of Zoomdata Upload API.

### Prerequisites ###

You will need `sbt` for this project. See [Installing sbt](http://www.scala-sbt.org/1.x/docs/Setup.html) for the setup instructions.

Start SBT
---------
```bash
$ sbt
```

Run all simulations
-------------------

```bash
> gatling:test
```

Run a single simulation
-----------------------

```bash
> gatling:testOnly simulations.UploadApiSimulation
```

List all tasks
--------------------

```bash
> tasks gatling -v
```

