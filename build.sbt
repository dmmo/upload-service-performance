

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.zoomdata",
      scalaVersion := "2.12.3",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "upload-service-performance",
    libraryDependencies ++= Seq(
      "com.typesafe"               %  "config"          % "1.3.2",
      "com.typesafe.scala-logging" %% "scala-logging"   % "3.7.2",
      "ch.qos.logback"             %  "logback-classic" % "1.2.3",

      "org.scalatest"              %% "scalatest"       % "3.0.3" % "test",
      "io.gatling"                 %  "gatling-test-framework"    % "2.3.0" % "test",
      "io.gatling.highcharts"      %  "gatling-charts-highcharts" % "2.3.0" % "test"
    )
  )

enablePlugins(GatlingPlugin)
